import { createRouter, createWebHistory } from 'vue-router';
import HomePage from './pages/HomePage.vue';

export default createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomePage,
    },
    {
      path: "/buttons",
      name: "buttons",
      meta: {
        title: 'Buttons',
      },
      component: () => import('./pages/ButtonsPage.vue'),
    },
    {
      path: "/dialogs",
      name: "dialogs",
      meta: {
        title: 'Dialogs',
      },
      component: () => import('./pages/DialogsPage.vue'),
    },
  ],
});

