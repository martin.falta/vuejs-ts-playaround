import { createApp, defineAsyncComponent } from 'vue'
import './style.css'
import App from './App.vue'
import router from './router';

const ContentCard = defineAsyncComponent(() => import('./components/layout/ContentCard.vue'));
const BaseButton = defineAsyncComponent(() => import('./components/ui/BaseButton.vue'));
const BaseDialog = defineAsyncComponent(() => import('./components/ui/BaseDialog.vue'));

const app = createApp(App)
  .use(router);

app.component('BaseButton', BaseButton);
app.component('ContentCard', ContentCard);
app.component('BaseDialog', BaseDialog);

app.mount('#app');
